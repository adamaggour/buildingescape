// Copyright Aggour Studios 2018

#include "OpenDoor.h"
#include "Engine/Classes/Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h"

#define OUT


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();   //door that opens, get the owning Actor

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing PressurePlate (TriggerVolume)\n"), *GetOwner()->GetName());
			return;
	}
	
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// poll TriggerVolume every frame
	if (GetTotalMassOfActorsOnPlate() > TriggerMass ) 
	{
		OnOpen.Broadcast();
		//LastDoorOpenTime = GetWorld()->GetTimeSeconds();
	}
	
	//close
	else
	{
		//CloseDoor();
		OnClose.Broadcast();
	}
	
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float TotalMass = 0.0f;

	//Find all overlapping Actors
	TArray<AActor*> OverlappingActors;
	if (!PressurePlate)
	{
		return 0.0f;
	}
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);

	//Iterate through and add their masses
	for (const auto* AnActor : OverlappingActors) 
	{
		
		// get Actor's mass and add to TotalMass
		TotalMass += AnActor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return TotalMass;
}